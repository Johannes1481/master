package E2E_Testing;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class MomentumInvControlMethod {
	WebDriver driver;
	
	Thread thread = new Thread();
	ReadUsers readUsers = new ReadUsers();
	
	
	String NAME = "name";
	String PASSWORD = "password";
	String EMAILADDRESS = "emailAddress";
	int i = 0;
	
	@Test
	public void launchBrowser()
	{

		//System.setProperty("webdriver.ie.driver", "C:\\Automation\\Drivers\\IEDriverServer.exe");
			System.setProperty("webdriver.chrome.driver", "C:\\Automation\\Drivers\\chromedriver_96.exe");
		    //driver = new InternetExplorerDriver();  
		    driver = new ChromeDriver();
		    driver.manage().window().maximize();
		    
		    driver.get("https://assessment-e2e-simple-login.herokuapp.com/"); 
	}
	
	public void getUserDetails()
	{
		String users[] = readUsers.returnUserDetails();
		NAME = users[i];
		EMAILADDRESS = users[i+1];
		PASSWORD = users[i+2];
		
	}
	
	@Test
	public void loginUser() throws InterruptedException
	{
		  getUserDetails(); //Initializing values
		  if(driver.findElement(By.linkText("Login")).isDisplayed())
		  {
			  driver.findElement(By.linkText("Login")).click();
		  }
		  
		  thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id=\"email\"]")).click();
		  driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(EMAILADDRESS);
		  driver.findElement(By.xpath("//*[@id=\"password\"]")).click();
		  driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(PASSWORD);
	  
		  thread.sleep(3000);
		  //Clicking submit button
		  driver.findElement(By.xpath("/html/body/form/fieldset/button[2]")).click();
		  
		  //checking if firstime user login successfully
		  if(driver.findElement(By.xpath("/html/body/div[1]/h2")).isDisplayed())
		  {
			  logout();
		  }
	}
	@Test
	public void validateLogintoRegister() throws InterruptedException
	{
		thread.sleep(3000);
		if(driver.findElement(By.linkText("login again")).isDisplayed())
		{
			driver.findElement(By.linkText("login again")).click();
			registerUser();
		}else {
			logout();
		}
	}
	public void logout() throws InterruptedException
	{
		thread.sleep(3000);
		if(driver.findElement(By.linkText("logout")).isDisplayed())
		{
			driver.findElement(By.linkText("logout")).click();
		}
		
	}
	
	
	
	public void registerUser() throws InterruptedException {		
	
		  thread.sleep(3000);
		  if (driver.findElement(By.linkText("register")).isDisplayed())
		  {
			  driver.findElement(By.linkText("register")).click();
		  }
		  thread.sleep(3000);
		  
		  if(driver.findElement(By.xpath("//*[@id=\"username\"]")).isDisplayed())
		  {
			  driver.findElement(By.xpath("//*[@id=\"username\"]")).click();
			  driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys(NAME);
		  }
		  
		  driver.findElement(By.xpath("//*[@id=\"email\"]")).click();
		  driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(EMAILADDRESS);
		  
		  driver.findElement(By.xpath("//*[@id=\"password\"]")).click();
		  driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(PASSWORD);		  
		  
		  thread.sleep(5000);
		 
		  driver.findElement(By.xpath("/html/body/form/fieldset/button[2]")).click();  //Clicking the submit button
		  
		  thread.sleep(3000);
		  loginRegisteredUser();
		  
	}
	public void loginRegisteredUser() throws InterruptedException
	{
		driver.findElement(By.linkText("login")).click();
		
		thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"email\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(EMAILADDRESS);
		  
		driver.findElement(By.xpath("//*[@id=\"password\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(PASSWORD);	
		
		thread.sleep(3000);
		 driver.findElement(By.xpath("/html/body/form/fieldset/button[2]")).click(); 
		
		logout();
		
	}
	
	@AfterTest
	public void closeBrowser() throws InterruptedException
	{
		thread.sleep(5000);
		driver.close();
	}
}
